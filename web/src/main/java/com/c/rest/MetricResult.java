package com.c.rest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "MetricResult")
public class MetricResult {
    @XmlElement(name = "storeName", required = true)
    private String storeName;
    @XmlElement(name = "total", required = true)
    private Double total;
    @XmlElement(name = "iso8601Date", required = true)
    private String iso8601Date;

    public MetricResult(){

    }
    public MetricResult(String storeName, Double total, String zonedDateTime) {

        this.storeName = storeName;
        this.total = total;
    }

    public String getIso8601Date() {
        return iso8601Date;
    }

    public void setIso8601Date(String iso8601Date) {
        this.iso8601Date = iso8601Date;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

}
