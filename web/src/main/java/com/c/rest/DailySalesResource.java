package com.c.rest;

import com.c.core.DailySalesService;
import com.c.core.model.MetricInfo;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
@Path("/dailysales")
public class DailySalesResource {
    private DailySalesService dailySalesService = DailySalesService.instance();
    @GET
    @Consumes(MediaType.TEXT_PLAIN)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/{iso8601Date}")
    public Response dailySales(@PathParam("iso8601Date") String iso8601Date){
        try {
            List<MetricInfo> metricInfos = dailySalesService.dailySales(ZonedDateTime.parse(iso8601Date));
            if (metricInfos.size() == 0){
                return Response.status(Response.Status.NOT_FOUND).build();
            }
            return Response.ok(toMetricResults(metricInfos)).build();
        } catch (Exception e) {
            return Response.serverError().build();
        }
    }

    private MetricResult[] toMetricResults(List<MetricInfo> metricInfos) {
        List<MetricResult> metricResults = new ArrayList<>(metricInfos.size());
        metricInfos.forEach(m-> metricResults.add(new MetricResult(m.getStoreName(), m.getTotal(),
                m.getZonedDateTime().format(DateTimeFormatter.ISO_INSTANT))));
        return metricResults.toArray(new MetricResult[metricResults.size()]);
    }

}
