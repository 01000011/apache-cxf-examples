package com.c.core;

import com.c.core.model.MetricInfo;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
public class DailySalesService {
    private Persistence persistence = HbasePersistence.instance();
    public static DailySalesService instance() {
        return DailySalesServiceHelper.INSTANCE;
    }

    public List<MetricInfo> dailySales(ZonedDateTime zonedDateTime) throws ApplicationException {
        try {
            return persistence.dailySales(zonedDateTime);
        } catch (PersistenceException e) {
            throw new ApplicationException(e);
        }
    }

    private static class DailySalesServiceHelper {
        public static final DailySalesService INSTANCE = new DailySalesService();
    }
}
