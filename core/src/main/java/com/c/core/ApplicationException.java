package com.c.core;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
public class ApplicationException extends Exception {
    public ApplicationException(Exception e) {
        super(e);
    }
}
