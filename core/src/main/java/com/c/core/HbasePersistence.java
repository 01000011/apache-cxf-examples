package com.c.core;

import com.c.core.model.MetricInfo;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.Bytes;

import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
public class HbasePersistence implements Persistence {
    private static final Logger LOGGER = Logger.getLogger(HbasePersistence.class.getName());
    public static final TableName TABLE_SALES = TableName.valueOf(Bytes.toBytes("metric:sales"));
    public static final byte[] COLUMN_FAMILY_SALES = Bytes.toBytes("d");
    private Connection connection;

    private HbasePersistence() {
        try {
            connection = ConnectionFactory.createConnection(HBaseConfiguration.create());
        } catch (Exception e) {
            LOGGER.severe(e.getMessage());
        }
    }

    private String createRowKey(ZonedDateTime zonedDateTime) {
        return String.format("%s%s%s", zonedDateTime.getYear(), zonedDateTime.getDayOfMonth(),
                zonedDateTime.getMonthValue());
    }

    @Override
    public List<MetricInfo> dailySales(ZonedDateTime zonedDateTime) throws PersistenceException {
        List<MetricInfo> metricInfos = new ArrayList<>();
        try (Table table = connection.getTable(TABLE_SALES)) {
            Result result = table.get(new Get(Bytes.toBytes(createRowKey(zonedDateTime))));
            if(!result.isEmpty()) {
                metricInfos.addAll(result.getFamilyMap(COLUMN_FAMILY_SALES).entrySet().stream().map(sale -> new MetricInfo(zonedDateTime,
                        Bytes.toDouble(sale.getValue()), Bytes.toString(sale.getKey()))).collect(Collectors.toList()));
            }
        } catch (Exception e) {
            throw new PersistenceException(e);
        }
        return metricInfos;
    }

    public static Persistence instance() {
        return HbasePersistenceServiceHelper.INSTANCE;
    }

    private static class HbasePersistenceServiceHelper {
        public static HbasePersistence INSTANCE;

        static {
            try {
                INSTANCE = new HbasePersistence();
            } catch (Exception e) {
                LOGGER.severe(e.getMessage());
            }
        }
    }
}