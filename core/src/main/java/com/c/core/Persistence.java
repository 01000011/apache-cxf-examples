package com.c.core;

import com.c.core.model.MetricInfo;

import java.time.ZonedDateTime;
import java.util.List;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
public interface Persistence {
    List<MetricInfo> dailySales(ZonedDateTime zonedDateTime) throws PersistenceException;
}
