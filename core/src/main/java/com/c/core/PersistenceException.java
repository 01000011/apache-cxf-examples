package com.c.core;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
public class PersistenceException extends Exception {
    public PersistenceException(Exception e) {
        super(e);
    }
}
