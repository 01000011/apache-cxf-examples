package com.c.core.model;

import java.time.ZonedDateTime;

/**
 * Created by Courtney Harleston on 3/23/16.
 */
public class MetricInfo {
    private ZonedDateTime zonedDateTime;
    private Double total;
    private String storeName;

    public MetricInfo() {
    }

    public MetricInfo(ZonedDateTime zonedDateTime, Double total, String storeName) {
        this.zonedDateTime = zonedDateTime;
        this.total = total;
        this.storeName = storeName;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public ZonedDateTime getZonedDateTime() {
        return zonedDateTime;
    }

    public void setZonedDateTime(ZonedDateTime zonedDateTime) {
        this.zonedDateTime = zonedDateTime;
    }
}
